const express = require('express');
const app = express();
const router = require('./config/routes');
const mongoose = require('mongoose');

const port = process.env.PORT || 5000;

app.use(express.json());

mongoose.connect('mongodb://127.0.0.1:27017/job', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
}, function () {
  'use strict';
  console.log("Connection to MongoDB success");
}).catch(function (err) {
  'use strict';
  console.log("Error: " + err);
});

app.use('/', router);

app.listen(port, () => console.log(`Server is listening in port: ${port}`));