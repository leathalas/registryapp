const UserService = require('../services/UsersService');
const { validationResult } = require('express-validator');

module.exports = {
  async login(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array()[0].msg });
    }

    let params = {
      email: req.body.email,
      password: req.body.password,
    };

    const data = await UserService.connectUser(params);
    res.json(data);
  },

  async register(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array()[0].msg });
    }

    let params = {
      email: req.body.email,
      company: req.body.company,
      password: req.body.password,
    };

    const data = await UserService.createUser(params);
    res.json(data);
  }
};