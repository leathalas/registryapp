const ExpenseService = require('../services/ExpenseService');

module.exports = {
  async createExpense(req, res, next) {
    const params = req.body;
    const data = await ExpenseService.createExpense(params);
    res.json(data);
  },

  async getAll(req, res, next) {
    const { query: { filter = null, userId = null } = {} } = req;
    const data = await ExpenseService.findAll(filter, userId);
    res.json(data);
  },

  async deleteExpense(req, res, next) {
    const params = req.body;
    const data = await ExpenseService.deleteExpense(params);
    res.json(data);
  },

  async updateExpense(req, res, next) {
    const params = req.body;
    const data = await ExpenseService.updateExpense(params);
    res.json(data);
  }
};

