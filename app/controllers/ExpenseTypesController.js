const ExpenseTypesService = require('../services/ExpenseTypesService');

module.exports = {
  async createType(req, res, next) {
    const params = req.body;
    const data = await ExpenseTypesService.createType(params);
    res.json(data);
  },

  async getAll(req, res, next) {
    const params = req.query;
    const data = await ExpenseTypesService.findAll(params);
    res.json(data);
  },

  async updateType(req, res, nect) {
    const params = req.body;
    const data = await ExpenseTypesService.updateType(params);
    res.json(data);
  },

  async deleteType(req, res, next) {
    const params = req.body;
    const data = await ExpenseTypesService.deleteType(params);
    res.json(data);
  },
};

