const ExpenseService = {
  createExpense: function (params) {
    return createExpense(params);
  },
  findAll: function (filter, userId) {
    return findAll(filter, userId);
  },
  deleteExpense: function (params) {
    return deleteExpense(params);
  },
  updateExpense: function (params) {
    return updateExpense(params);
  }
};

module.exports = ExpenseService;

const Expenses = require('../models/ExpenseModel').Expenses;
const ExpenseTypes = require('../models/ExpenseTypesModel').ExpenseTypes;
const Users = require('../models/UserModel').Users;

async function updateExpense(params) {
  if (!params.data.date) {
    return { data: null, errors: 'Data negali būti tuščia!' };
  }
  if (!params.data.provider) {
    return { data: null, errors: 'Tiekėjo laukelis negali būti tuščias!' };
  }
  if (!params.data.type) {
    return { data: null, errors: 'Dokumento tipas negali būti tuščias!' };
  }
  if (!params.data.docNumber) {
    return { data: null, errors: 'Dokumento numeris negali būti tuščias!' };
  }
  if (params.data.value.length <= 0) {
    return { data: null, errors: 'Prašome patikslinti sumą!' };
  }
  if (!params.userId) {
    return { data: null, errors: 'Negalite atlikti veiksmų neprisijūngus!' };
  }

  params.data.value = Number.parseFloat(params.data.value);
  params.data.value = params.data.value.toFixed(2);
  params.data.value = Number.parseFloat(params.data.value);

  if (params.data.value < 0 || typeof params.data.value !== "number" || Number.isNaN(params.data.value)) {
    return { data: null, errors: 'Prašome patikslinti sumą!' };
  }
  const user = await Users.findOne({ _id: params.userId });
  if (!user) {
    return { data: null, errors: 'Vartotojas nerastas!' };
  }
  const updateData = {
    date: params.data.date,
    type: params.data.type,
    provider: params.data.provider,
    docNumber: params.data.docNumber,
    value: params.data.value,
  };
  const data = await Expenses.updateOne({ _id: params.data.id, owner: params.userId }, updateData);
  if (!data) {
    return { data: null, errors: 'Duombazėje įvyko klaida!' };
  }
  return { data: data, errors: '' };
}

async function createExpense(params) {
  if (!params.data.date) {
    return { data: null, errors: 'Data negali būti tuščia!' };
  }
  if (!params.data.provider) {
    return { data: null, errors: 'Tiekėjo laukelis negali būti tuščias!' };
  }
  if (!params.data.type) {
    return { data: null, errors: 'Dokumento tipas negali būti tuščias!' };
  }
  if (!params.data.docNumber) {
    return { data: null, errors: 'Dokumento numeris negali būti tuščias!' };
  }
  if (params.data.value.length <= 0) {
    return { data: null, errors: 'Prašome patikslinti sumą!' };
  }
  if (!params.owner) {
    return { data: null, errors: 'Negalite atlikti veiksmų neprisijūngus!' };
  }
  const user = await Users.findOne({ _id: params.owner });
  if (!user) {
    return { data: null, errors: 'Vartotojas nerastas!' };
  }
  params.data.owner = params.owner;

  params.data.value = Number.parseFloat(params.data.value);
  params.data.value = params.data.value.toFixed(2);
  params.data.value = Number.parseFloat(params.data.value);

  if (params.data.value < 0 || typeof params.data.value !== "number" || Number.isNaN(params.data.value)) {
    return { data: null, errors: 'Prašome patikslinti sumą!' };
  }

  const data = await Expenses.create(params.data);
  if (!data) {
    return { data: null, errors: 'Įvyko serverio klaida' };
  }
  return { data: data, errors: '' };
}

async function deleteExpense(params) {
  if (!params.id) {
    return { data: null, errors: 'Išlaidų ID negali būti tuščias!' };
  }
  if (!params.userId) {
    return { data: null, errors: 'Negalite atlikti veiksmų neprisijūngus!' };
  }
  const expense = Expenses.findOne({ _id: params.id });
  if (!expense) {
    return { data: null, errors: 'Tokio išlaidų įrašo nėra!' };
  }
  const data = await Expenses.deleteOne({ _id: params.id, owner: params.userId });
  return { data: data, errors: '' };
}

async function findAll(filter = null, userId = null) {
  const data = [];
  const sort = { sort: { date: -1 } };
  try {
    let filters = { owner: userId };
    if (filter) {
      filters = Object.assign({}, JSON.parse(filter));
      filters = Object.assign(filters, { owner: userId });
    }
    const expenses = await Expenses.find(filters, null, sort);
    for (let i = 0; i < expenses.length; i++) {
      let type = await ExpenseTypes.findOne({ _id: expenses[i].type });
      data.push({
        _id: expenses[i]._id,
        date: expenses[i].date,
        docNumber: expenses[i].docNumber,
        provider: expenses[i].provider,
        value: expenses[i].value,
      });
      if (type) {
        data[i].type = type.name;
        data[i].typeid = type.id;
      }
    }
    return { data: data, errors: '' };
  } catch (err) {
    return { data: null, errors: err }
  }
}