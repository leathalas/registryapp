const ExpenseTypesService = {
  createType: function (params) {
    return createType(params);
  },
  findAll: function (params) {
    return findAll(params);
  },
  updateType: function (params) {
    return updateType(params);
  },
  deleteType: function (params) {
    return deleteType(params);
  }
};

module.exports = ExpenseTypesService;

const ExpenseTypes = require('../models/ExpenseTypesModel').ExpenseTypes;
const Users = require('../models/UserModel').Users;

async function updateType(params) {
  if (!params.data.name) {
    return { data: null, errors: 'Išlaidų tipo pavadinimas negali būti tuščias!' };
  }
  if (!params.data.id) {
    return { data: null, errors: 'Išlaidų tipo ID negali būti tuščias' };
  }
  if (!params.userId) {
    return { data: null, errors: 'Negalite atlikti veiksmų neprisijūngus!' };
  }
  const user = await Users.findOne({ _id: params.userId });
  if (!user) {
    return { data: null, errors: 'Vartotojas nerastas!' };
  }
  const type = await ExpenseTypes.findOne({ name: params.data.name });
  if (type && (type.name !== params.data.name && type.owner === params.userId)) {
    return { data: null, errors: 'Išlaidų tipas jau egzistuoja!' };
  }
  const updateData = {
    name: params.data.name,
    enabled: params.data.enabled,
  };
  const data = await ExpenseTypes.updateMany({ _id: params.data.id }, updateData);
  return { data: data, errors: '' };
}

async function createType(params) {
  if (!params.data.name) {
    return { data: null, errors: 'Išlaidų tipo pavadinimas negali būti tuščias!' };
  }
  if (!params.userId) {
    return { data: null, errors: 'Negalite atlikti veiksmų neprisijūngus!' };
  }
  const user = await Users.findOne({ _id: params.userId });
  if (!user) {
    return { data: null, errors: 'Vartotojas nerastas!' };
  }
  params.data.owner = params.userId;
  const type = await ExpenseTypes.findOne({ name: params.data.name });
  if (type && type.owner === params.userId) {
    return { data: null, errors: 'Išlaidų tipas jau egzistuoja!' };
  }
  const data = await ExpenseTypes.create(params.data);
  if (!data) {
    return { data: null, errors: 'Įvyko serverio klaida' };
  }
  return { data: data, errors: '' };
}

async function findAll(params) {
  let data = null;
  const sort = { sort: { enabled: -1, name: 1 } };
  if (params.allEnabled) {
    data = await ExpenseTypes.find({ enabled: true, owner: params.userId });
  } else {
    data = await ExpenseTypes.find({ owner: params.userId }, null, sort);
  }
  return { data: data, errors: '' };
}

async function deleteType(params) {
  if (!params.id) {
    return { data: null, errors: 'Išlaidų tipo ID negali būti tuščias!' };
  }
  const type = ExpenseTypes.findOne({ _id: params.id, owner: params.userId });
  if (!type) {
    return { data: null, errors: 'Tokio išlaidų įrašo nėra!' };
  }
  const data = await ExpenseTypes.deleteOne({ _id: params.id, owner: params.userId });
  return { data: data, errors: '' };
}