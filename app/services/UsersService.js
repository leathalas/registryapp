const UserService = {
  createUser: function (params) {
    return createUser(params);
  },
  connectUser: function (params) {
    return connectUser(params);
  },
};

module.exports = UserService;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { secretKey } = require('../../config/globals');
const Users = require('../models/UserModel').Users;

async function createUser(params) {
  let hashed = await bcrypt.hash(params.password, 15);

  const userCreate = {
    email: params.email,
    company: params.company,
    password: hashed,
  };

  let user = await Users.findOne({ email: params.email });
  if (user) {
    return { data: null, errors: "Vartotojas su tokiu el. paštu jau egzistuoja!" };
  }

  let company = await Users.findOne({ company: params.company });
  if (company) {
    return { data: null, errors: "Vartotojas su tokiu kompanijos pavadinimu jau egzistuoja!" };
  }

  const created = await Users.create(userCreate);

  if (!created) {
    return { data: null, errors: "Duombazės klaida!" };
  }

  const login = {
    email: params.email,
    password: params.password,
  };

  return connectUser(login);
}

async function connectUser(params) {
  if (!params.email || !params.password) {
    return { errors: "Vartotojo prisijungimo duomenys neteisingi!" };
  }
  let user = await Users.findOne({ email: params.email });
  if (!user) {
    return { errors: "Vartotojo prisijungimo duomenys neteisingi!" };
  }
  let login = await bcrypt.compare(params.password, user.password);
  if (!login) {
    return { errors: "Vartotojo prisijungimo duomenys neteisingi!" };
  }
  const token = await jwt.sign({ user }, secretKey);
  if (!token) {
    return { errors: "Vartotojo prisijungimo duomenys neteisingi!" };
  }
  return { token: token, errors: "" };
}
