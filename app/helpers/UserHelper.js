const { body } = require('express-validator');

module.exports = {
  validate: function (method) {
    switch (method) {
      case 'UserRegister':
        return [
          body('email', 'Prašome patikslinti el. paštą!').exists().isEmail(),
          body('company', 'Įmonės pavadinimas būtinas').exists(),
          body('password', 'Slaptažodžio ilgis turi būti nuo: 8-50 prašome pasirinkti kitą slaptažodį').isLength({ min: 8, max: 50 })
        ];
    }
  }
};