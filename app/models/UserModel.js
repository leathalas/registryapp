const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  company: {
    type: String,
    required: true,
  },
  password: {
    type: String,
  },
});

module.exports = { Users: mongoose.model('Users', UsersSchema) };