const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExpenseSchema = new Schema({
  docNumber: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
  provider: {
    type: String,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
  owner: {
    type: String,
    required: true,
  },
});

module.exports = { Expenses: mongoose.model('Expenses', ExpenseSchema) };