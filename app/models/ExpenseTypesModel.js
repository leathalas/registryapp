const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExpenseTypeSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  enabled: {
    type: Boolean,
    default: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  owner: {
    type: String,
    required: true,
  },
});

module.exports = { ExpenseTypes: mongoose.model('ExpenseTypes', ExpenseTypeSchema) };