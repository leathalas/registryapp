import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PropTypes from 'prop-types';
import TopNav from './components/Partials/TopNav';
import 'rsuite/dist/styles/rsuite-default.css';
import 'react-notifications-component/dist/theme.css';
import './styles/template.css';
import { Container } from 'rsuite';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.getRoutes = this.getRoutes.bind(this);
  }

  getRoutes() {
    const { routes } = this.props;
    const { sessionStorage } = window;
    const token = sessionStorage.getItem('token');
    const appRoutes = [];
    routes.forEach((route, i) => {
      if ((route.authenticated && token) || (!route.authenticated && !token)) {
        appRoutes.push(<Route key={i} {...route} />);
      }
    });
    return appRoutes;
  }

  render() {
    return (
      <Router>
        <div className="show-container">
          <Container>
            <TopNav />
            <Switch>{this.getRoutes()}</Switch>
          </Container>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  location: PropTypes.object,
  routes: PropTypes.array.isRequired,
};

export default App;