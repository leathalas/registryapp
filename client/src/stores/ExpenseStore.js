import { decorate, action, observable, runInAction } from 'mobx';
import axios from 'axios';

const newType = {
  name: '',
  enabled: true,
};

const newExpense = {
  date: new Date(),
  type: null,
  docNumber: '',
  provider: '',
  value: 0,
};

const date = new Date();

class ExpenseStore {
  constructor() {
    this.expense = newExpense;
    this.expenseType = newType;
    this.expenses = [];
    this.expenseTypes = [];
    this.total = 0;
    this.selectedTypes = [];
    this.selectedAllTypes = false;
    this.selectedDate = [
      new Date(date.getFullYear(), date.getMonth(), 1),
      new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59),
    ];
    this.expenseFilter = {
      date: {
        $gte: this.selectedDate[0],
        $lte: this.selectedDate[1],
      },
    };
  }

  setDateSelection(date) {
    this.selectedDate = date;
  }

  setTypeSelection(values) {
    this.selectedTypes = Object.assign([], values);
    this.selectedAllTypes = this.expenseTypes.length === this.selectedTypes.length;
  }

  setExpenseTypeFormData(name) {
    this.expenseType.name = name;
  }

  setExpenseTypeCheckboxValue(field) {
    this.expenseType[field] = !this.expenseType[field];
  }

  setExpenseFormData(field, value) {
    this.expense[field] = value;
  }

  setExpenseFormDate(date) {
    this.expense.date = date;
  }

  setExpenseFormType(type) {
    this.expense.type = type;
  }

  setExpenseEditData(data) {
    this.expense = data;
    this.expense.date = new Date(data.date);
    this.expense.type = data.typeid;
  }

  setExpenseTypeEditData(data) {
    this.expenseType = data;
  }

  countTotal() {
    this.total = 0;
    if (!this.expenses) {
      return;
    }
    this.expenses.forEach(expense => {
      this.total += expense.value;
    });
  }

  updateExpenseData(userId) {
    const opts = {
      data: this.expense,
      userId: userId,
    };
    return new Promise((resolve, reject) => {
      axios
        .put('/api/expense/update', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on update expense'));
              }
            });
          }
        });
    });
  }

  updateExpenseTypeData(userId) {
    const opts = {
      data: this.expenseType,
      userId: userId,
    };
    return new Promise((resolve, reject) => {
      axios
        .put('/api/expense/types/update', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on update expense'));
              }
            });
          }
        });
    });
  }

  deleteExpense(id, userId) {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/expense/delete', { id: id, userId: userId })
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on delete expense'));
              }
            });
          }
        });
    });
  }

  deleteExpenseType(id, userId) {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/expense/types/delete', { id: id, userId: userId })
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on delete expense'));
              }
            });
          }
        });
    });
  }

  createNewExpenseType(userId) {
    const opts = {
      data: this.expenseType,
      userId: userId,
    };
    return new Promise((resolve, reject) => {
      axios
        .post('/api/expense/types/create', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on create expense type'));
              }
            });
          }
        });
    });
  }

  resetExpenseType() {
    this.expenseType = newType;
  }

  resetExpense() {
    this.expense = newExpense;
  }

  createNewExpense(userId) {
    const opts = {
      data: this.expense,
      owner: userId,
    };
    return new Promise((resolve, reject) => {
      axios
        .post('/api/expense/create', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Error on create expense type'));
              }
            });
          }
        });
    });
  }

  loadAllExpenseTypes(userId) {
    const opts = {
      params: {
        userId: userId,
      },
    };
    return new Promise((resolve, reject) => {
      axios
        .get('/api/expense/types/findAll', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data = {} } = response;
              if (data) {
                this.expenseTypes = data.data;
                resolve(data);
              } else {
                reject(new Error('Error on find expense types'));
              }
            });
          }
        });
    });
  }

  loadAllExpenses(filter = null, userId) {
    this.expenseFilter = filter;
    const opts = {
      params: {
        filter: this.expenseFilter,
        userId: userId,
      },
    };
    return new Promise((resolve, reject) => {
      axios
        .get('/api/expense/findAll', opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data = {} } = response;
              if (data) {
                this.expenses = data.data;
                resolve(data);
              } else {
                reject(new Error('Error on find expense types'));
              }
            });
          }
        });
    });
  }
}

decorate(ExpenseStore, {
  total: observable,
  expenseFilter: observable,
  expense: observable,
  expenseType: observable,
  expenses: observable,
  expenseTypes: observable,
  selectedTypes: observable,
  selectedAllTypes: observable,
  selectedDate: observable,
  setExpenseFormData: action,
  setExpenseFormDate: action,
  setExpenseFormType: action,
  setExpenseTypeFormData: action,
  setExpenseTypeCheckboxValue: action,
  createNewExpenseType: action,
  createNewExpense: action,
  loadAllExpenseTypes: action,
  loadAllExpenses: action,
  setExpenseEditData: action,
  deleteExpense: action,
  updateExpenseData: action,
  resetExpense: action,
  resetExpenseType: action,
  setExpenseTypeEditData: action,
  updateExpenseTypeData: action,
  countTotal: action,
  setTypeSelection: action,
  setDateSelection: action,
});

export default new ExpenseStore();