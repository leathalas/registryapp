import axios from 'axios';
import { runInAction, decorate, action, observable } from 'mobx';

const newUser = {
  email: '',
  company: '',
  password: '',
};

const oldUser = {
  email: '',
  password: '',
};

class UsersStore {
  constructor() {
    this.userRegisterData = Object.assign({}, newUser);
    this.userLoginData = Object.assign({}, oldUser);
    this.setRegisterData = this.setRegisterData.bind(this);
    this.setLoginData = this.setLoginData.bind(this);
  }

  userLogin() {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/user/login', this.userLoginData)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Login error'));
              }
            });
          }
        });
    });
  }

  userRegister() {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/user/register', this.userRegisterData)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Register error'));
              }
            });
          }
        });
    });
  }

  setRegisterData(data) {
    this.userRegisterData = Object.assign({}, this.userRegisterData, data);
  }

  setLoginData(data) {
    this.userLoginData = Object.assign({}, this.userLoginData, data);
  }
}

decorate(UsersStore, {
  userRegisterData: observable,
  userLoginData: observable,
  userRegister: action,
  userLogin: action,
  setRegisterData: action,
  setLoginData: action,
});

export default new UsersStore();