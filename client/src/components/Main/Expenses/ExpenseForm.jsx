import React, { Component } from 'react';
import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
  SelectPicker,
  IconButton,
  Icon,
  DatePicker,
  Schema,
  Col,
  Row,
} from 'rsuite';

const { StringType, ObjectType, NumberType } = Schema.Types;

class ExpenseForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: {
        sunday: 'S',
        monday: 'Pr',
        tuesday: 'A',
        wednesday: 'T',
        thursday: 'K',
        friday: 'Pn',
        saturday: 'Š',
        ok: 'Gerai',
        today: 'Šiandien',
        yesterday: 'Vakar',
        hours: 'Valandos',
        minutes: 'Minutės',
        seconds: 'Sekundės',
      },
      ranges: [],
    };
    this.expenseStore = this.props.expenseStore;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.formatExpenseTypes = this.formatExpenseTypes.bind(this);
    this.handleCheckPickerChange = this.handleCheckPickerChange.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
    this.model = Schema.Model({
      date: ObjectType()
        .isRequired('Prašom pasirinkti datą'),
      provider: StringType()
        .isRequired('Prašom įvesti tiekėją'),
      type: StringType()
        .isRequired('Prašom pasirinkti tipą'),
      docNumber: StringType()
        .isRequired('Prašom įvesti dokumento numerį'),
      value: NumberType()
        .isRequired('Prašom įvesti sumą'),
    });
    this.form = this.props.form;
  }

  componentDidMount() {
    this.expenseStore.loadAllExpenseTypes(this.userId);
    this.handleDateChange(new Date());
  }

  handleInputChange(value, event) {
    this.expenseStore.setExpenseFormData(event.target.name, value);
  }

  handleDateChange(date) {
    this.expenseStore.setExpenseFormDate(date);
  }

  handleCheckPickerChange(data) {
    this.expenseStore.setExpenseFormType(data);
  }

  formatExpenseTypes(expenseTypes) {
    let data = [];
    expenseTypes.forEach((expenseType) => {
      if (expenseType.enabled) {
        data.push({
          label: expenseType.name,
          value: expenseType._id,
        });
      }
    });
    return data;
  }

  render() {
    const { expenseTypes, expense } = this.props.expenseStore;
    return (
      <Form
        fluid
        ref={this.form}
        formValue={expense}
        model={this.model}
      >
        <FormGroup>
          <ControlLabel>Data</ControlLabel>
          <FormControl
            accepter={DatePicker}
            name='date'
            locale={this.state.locale}
            ranges={this.state.ranges}
            defaultValue={expense.date}
            onChange={this.handleDateChange.bind(this)}
          />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Tiekėjas</ControlLabel>
          <FormControl name="provider" onChange={this.handleInputChange.bind(this)} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Tipas</ControlLabel>
          <Row>
            <Col xs={6}>
              <FormControl
                accepter={SelectPicker}
                name="type"
                data={this.formatExpenseTypes(expenseTypes)}
                onChange={this.handleCheckPickerChange.bind(this)}
                appearance="default"
                placeholder="Pasirinkti tipą"
                defaultValue={expense.type}
              />
            </Col>
            <Col xs={18}>
              <IconButton
                icon={<Icon icon="plus-square" />}
                placement="left"
                href='/company/expense/types'
              >
                Įvesti naują
              </IconButton>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Dokumento numeris</ControlLabel>
          <FormControl name="docNumber" onChange={this.handleInputChange.bind(this)} />
        </FormGroup>
        <FormGroup>
          <ControlLabel>Suma</ControlLabel>
          <FormControl name="value" onChange={this.handleInputChange.bind(this)} />
        </FormGroup>
      </Form>
    );
  }
}

ExpenseForm.propTypes = {
  expenseStore: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
};

const enhance = compose(withRouter, inject('expenseStore'), observer);

export default enhance(ExpenseForm);