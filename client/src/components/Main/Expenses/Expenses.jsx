import React, { Component } from 'react';
import {
  Table,
  Content,
  Panel,
  Grid,
  Row,
  Col,
  IconButton,
  Icon,
  FlexboxGrid,
  DateRangePicker,
  Checkbox,
  CheckPicker,
  Button,
} from 'rsuite';

import CreateExpenseModal from './CreateExpenseModal';
import EditExpenseModal from './EditExpenseModal';
import { observer, inject } from 'mobx-react';
import { compose } from 'recompose';
import PropTypes from 'prop-types';

const { Column, Cell, HeaderCell } = Table;

class Expenses extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.createExpenseModal = React.createRef();
    this.editExpenseModal = React.createRef();
    this.openCreateExpenseModal = this.openCreateExpenseModal.bind(this);
    this.openEditExpenseModal = this.openEditExpenseModal.bind(this);
    this.formatExpenseData = this.formatExpenseData.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.formatChartData = this.formatChartData.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
    this.filterByDate = this.filterByDate.bind(this);
    this.filterByType = this.filterByType.bind(this);
    this.resetTypeFilter = this.resetTypeFilter.bind(this);
    this.getActionButtons = this.getActionButtons.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckAll = this.handleCheckAll.bind(this);
    this.formatExpenseTypes = this.formatExpenseTypes.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
    this.state = {
      locale: {
        sunday: 'S',
        monday: 'Pr',
        tuesday: 'A',
        wednesday: 'T',
        thursday: 'K',
        friday: 'Pn',
        saturday: 'Š',
        ok: 'Gerai',
        today: 'Šiandien',
        yesterday: 'Vakar',
        hours: 'Valandos',
        minutes: 'Minutės',
        seconds: 'Sekundės',
      },
      ranges: [],
    };
  }

  openCreateExpenseModal() {
    this.expenseStore.resetExpense();
    this.createExpenseModal.current.open();
  }

  openEditExpenseModal() {
    this.editExpenseModal.current.open();
  }

  componentDidMount() {
    const { expenseFilter } = this.expenseStore;
    this.expenseStore.loadAllExpenses(expenseFilter, this.userId);
    this.expenseStore.loadAllExpenseTypes(this.userId);
  }

  formatDate(date) {
    let dateFormat = '';
    if (date) {
      date = new Date(date);
      dateFormat += date.getFullYear() + '/';
      dateFormat += (date.getMonth() + 1) + '/';
      dateFormat += date.getDate();
    }
    return dateFormat;
  }

  formatExpenseData(expenses) {
    let data = [];
    if (!expenses) {
      return data;
    }
    expenses.forEach(expense => {
      let date = this.formatDate(expense.date);
      data.push({
        date: date,
        id: expense._id,
        typeid: expense.typeid,
        type: expense.type,
        provider: expense.provider,
        docNumber: expense.docNumber,
        value: expense.value,
      });
    });
    return data;
  }

  formatChartData(expenses) {
    let adjusted = [];
    adjusted.push(['Išlaidos', 'Išlaidos per laiko tarpą']);
    expenses.forEach(expense => {
      let exist = false;
      let index = -1;
      let type = expense.type;
      if (!type) {
        type = 'Neregistruoti';
      }
      for (let i = 0; i < adjusted.length; i++) {
        if (adjusted[i][0] === type) {
          exist = true;
          index = i;
        }
      }
      if (!exist) {
        adjusted.push([type, expense.value]);
      } else {
        adjusted[index][1] += expense.value;
      }
    });
    return adjusted;
  }

  handleRowClick(data, event) {
    if (event.target.innerText) {
      this.expenseStore.setExpenseEditData(data);
      this.openEditExpenseModal();
    }
  }

  handleDeleteItem(data) {
    this.expenseStore.deleteExpense(data.id, this.userId).then((data) => {
      if (data && !data.errors) {
        const { expenseFilter } = this.expenseStore;
        this.expenseStore.loadAllExpenses(expenseFilter, this.userId);
      } else {
        //TODO: notification
      }
    });
  }

  handleChange(value) {
    this.expenseStore.setTypeSelection(value);
  }

  formatExpenseTypes(expenseTypes) {
    let data = [];
    expenseTypes.forEach((expenseType) => {
      data.push({
        label: expenseType.name,
        value: expenseType._id,
      });
    });
    return data;
  }

  handleCheckAll(value, checked) {
    const { expenseTypes } = this.expenseStore;
    const expenseTypeIds = [];
    expenseTypes.forEach(expense => {
      expenseTypeIds.push(expense._id);
    });
    const nextValue = checked ? expenseTypeIds : [];
    this.expenseStore.setTypeSelection(nextValue);
  }

  filterByType() {
    const { expenseFilter, selectedTypes } = this.expenseStore;
    let filters = {};
    if (expenseFilter) {
      filters = Object.assign(filters, expenseFilter);
    }
    this.picker.close();
    if (selectedTypes.length > 0) {
      const filter = {
        type: {
          $in: selectedTypes,
        },
      };
      filters = Object.assign(filters, filter);
    } else {
      if (Object.keys(filters).indexOf('type') !== -1) {
        delete filters.type;
      }
    }
    this.expenseStore.loadAllExpenses(filters, this.userId);
  }

  filterByDate(date) {
    const { expenseFilter } = this.expenseStore;
    let filters = {};
    if (expenseFilter) {
      filters = Object.assign(filters, expenseFilter);
    }
    if (date.length > 0) {
      date[1].setHours(23, 59, 59);
      const filter = {
        date: {
          $gte: date[0],
          $lte: date[1],
        },
      };
      this.expenseStore.setDateSelection(date);
      filters = Object.assign(filters, filter);
    } else {
      if (Object.keys(filters).indexOf('date') !== -1) {
        delete filters.date;
        this.expenseStore.setDateSelection(undefined);
      }
    }
    this.expenseStore.loadAllExpenses(filters, this.userId);
  }

  resetTypeFilter() {
    this.expenseStore.setTypeSelection();
    this.filterByType();
  }

  getActionButtons() {
    const {
      expenseTypes,
      selectedTypes,
      selectedAllTypes = false,
      selectedDate = null,
    } = this.expenseStore;
    const buttons = [];
    buttons.push(
      <FlexboxGrid.Item key="date-filter">
        <DateRangePicker
          locale={this.state.locale}
          ranges={this.state.ranges}
          placeholder="Filtruoti pagal datą"
          placement="bottomEnd"
          defaultValue={selectedDate}
          onChange={this.filterByDate.bind(this)}
        />
      </FlexboxGrid.Item>
    );
    buttons.push(
      <FlexboxGrid.Item key="type-filter">
        <CheckPicker
          placeholder="Filtruoti pagal tipą"
          ref={ref => {
            this.picker = ref;
          }}
          style={{ width: 170 }}
          placement="bottomEnd"
          value={selectedTypes}
          data={this.formatExpenseTypes(expenseTypes)}
          checked={this.formatExpenseTypes(expenseTypes)}
          onChange={this.handleChange}
          onClean={this.resetTypeFilter}
          renderExtraFooter={() => (
            <FlexboxGrid align='middle' justify="space-between">
              <FlexboxGrid.Item>
                <Checkbox
                  inline
                  onChange={this.handleCheckAll}
                  checked={selectedAllTypes}
                >
                  Pažymėti visus
              </Checkbox>
              </FlexboxGrid.Item>
              <FlexboxGrid.Item>
                <Button
                  appearance="primary"
                  style={{ margin: 5 }}
                  size="sm"
                  onClick={this.filterByType.bind(this)}
                >
                  Gerai
                </Button>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          )}
        />
      </FlexboxGrid.Item>
    );
    buttons.push(
      <FlexboxGrid.Item key="add-expense">
        <IconButton
          icon={<Icon icon="plus-square" />}
          placement="left"
          onClick={this.openCreateExpenseModal}
        >
          Registruoti išlaidas
      </IconButton>
      </FlexboxGrid.Item>
    );
    return buttons;
  }

  render() {
    const { expenses } = this.expenseStore;
    return (
      <Content>
        <CreateExpenseModal ref={this.createExpenseModal} />
        <EditExpenseModal ref={this.editExpenseModal} />
        <FlexboxGrid justify='end' style={{ marginRight: 25, marginTop: 10, marginBottom: 0 }}>
          {this.getActionButtons()}
        </FlexboxGrid>
        <Grid fluid>
          <Row className="show-grid">
            <Col xs={24}>
              <Panel header="Išlaidų sąrašas" bordered>
                <Table
                  data={this.formatExpenseData(expenses)}
                  height={600}
                  onRowClick={this.handleRowClick.bind(this)}
                >
                  <Column align="center" flexGrow={1} name="asd">
                    <HeaderCell>Data</HeaderCell>
                    <Cell dataKey="date" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Tipas</HeaderCell>
                    <Cell dataKey="type" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Tiekėjas</HeaderCell>
                    <Cell dataKey="provider" />
                  </Column>
                  <Column align="center" flexGrow={1} minWidth={150}>
                    <HeaderCell>Dokumento numeris</HeaderCell>
                    <Cell dataKey="docNumber" />
                  </Column>
                  <Column align="center" fixed="right" flexGrow={1}>
                    <HeaderCell>Suma</HeaderCell>
                    <Cell dataKey="value" />
                  </Column>
                  <Column align="center" width={120} fixed="right" verticalAlign="middle">
                    <HeaderCell>Veiksmai</HeaderCell>
                    <Cell className="link-group" style={{ padding: 0 }}>
                      {rowData => {
                        return (
                          <IconButton
                            icon={<Icon icon="trash2" style={{ color: '#f44336' }} />}
                            circle size="sm"
                            name="delete"
                            onClick={this.handleDeleteItem.bind(this, rowData)}
                          />
                        );
                      }}
                    </Cell>
                  </Column>
                </Table>
              </Panel>
            </Col>
          </Row>
        </Grid>
      </Content >
    );
  }
}

Expenses.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(Expenses);