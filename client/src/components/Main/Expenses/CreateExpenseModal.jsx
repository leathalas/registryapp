import React, { Component } from 'react';
import { compose } from 'recompose';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';

import Modal from '../../Partials/Modal';
import ExpenseFrom from './ExpenseForm';
import ReactNotification from 'react-notifications-component';
import { store } from 'react-notifications-component';

class CreateExpenseModal extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.modal = React.createRef();
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.onSave = this.onSave.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
    this.form = React.createRef();
  }

  open() {
    this.modal.current.open();
  }

  close() {
    this.modal.current.close();
  }

  onSave() {
    if (!this.form.current.check()) {
      return;
    }
    this.expenseStore.createNewExpense(this.userId).then(data => {
      if (data && !data.errors) {
        const { expenseFilter } = this.expenseStore;
        this.expenseStore.loadAllExpenses(expenseFilter, this.userId);
        this.expenseStore.resetExpense();
        this.close();
      } else {
        store.addNotification({
          title: "Klaida!",
          message: data.errors,
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 2000,
            onScreen: true,
          },
        });
      }
    });
  }

  render() {
    return (
      <div>
        <ReactNotification />
        <Modal
          title="Išlaidų registracija"
          ref={this.modal}
          onSubmit={this.onSave}
          submitName="Išsaugoti"
          closeName='Uždaryti'
        >
          <ExpenseFrom form={this.form} />
        </Modal>
      </div>
    );
  }
}

CreateExpenseModal.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(CreateExpenseModal);
