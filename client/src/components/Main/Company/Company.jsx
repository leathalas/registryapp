import React, { Component } from 'react';
import {
  Table,
  Content,
  Panel,
  Grid,
  Row,
  Col,
  Loader,
  FlexboxGrid,
  DateRangePicker,
  Checkbox,
  CheckPicker,
  Button,
} from 'rsuite';
import Chart from 'react-google-charts';
import { observer, inject } from 'mobx-react';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import EditExpenseModal from '../Expenses/EditExpenseModal';

const { Column, Cell, HeaderCell } = Table;

class Company extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.formatExpenseData = this.formatExpenseData.bind(this);
    this.formatExpenseTypes = this.formatExpenseTypes.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.formatChartData = this.formatChartData.bind(this);
    this.editExpenseModal = React.createRef();
    this.openEditExpenseModal = this.openEditExpenseModal.bind(this);
    this.getActionButtons = this.getActionButtons.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckAll = this.handleCheckAll.bind(this);
    this.filterByDate = this.filterByDate.bind(this);
    this.filterByType = this.filterByType.bind(this);
    this.resetTypeFilter = this.resetTypeFilter.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
    this.state = {
      locale: {
        sunday: 'S',
        monday: 'Pr',
        tuesday: 'A',
        wednesday: 'T',
        thursday: 'K',
        friday: 'Pn',
        saturday: 'Š',
        ok: 'Gerai',
        today: 'Šiandien',
        yesterday: 'Vakar',
        hours: 'Valandos',
        minutes: 'Minutės',
        seconds: 'Sekundės',
      },
      ranges: [],
    };
  }

  openEditExpenseModal() {
    this.editExpenseModal.current.open();
  }

  handleRowClick(data, event) {
    if (event.target.innerText) {
      this.expenseStore.setExpenseEditData(data);
      this.openEditExpenseModal();
    }
  }

  componentDidMount() {
    const { expenseFilter } = this.expenseStore;
    this.expenseStore.loadAllExpenses(expenseFilter, this.userId).then(() => {
      this.expenseStore.countTotal();
    });
    this.expenseStore.loadAllExpenseTypes(this.userId);
  }

  formatDate(date) {
    let dateFormat = '';
    if (date) {
      date = new Date(date);
      dateFormat += date.getFullYear() + '/';
      dateFormat += (date.getMonth() + 1) + '/';
      dateFormat += date.getDate();
    }
    return dateFormat;
  }

  formatExpenseData(expenses) {
    let data = [];
    if (!expenses) {
      return;
    }
    expenses.forEach(expense => {
      let date = this.formatDate(expense.date);
      data.push({
        date: date,
        id: expense._id,
        typeid: expense.typeid,
        type: expense.type,
        provider: expense.provider,
        docNumber: expense.docNumber,
        value: expense.value,
      });
    });
    return data;
  }

  formatChartData(expenses) {
    let adjusted = [];
    adjusted.push(['Išlaidos', 'Išlaidos per laiko tarpą']);
    if (!expenses) {
      return;
    }
    expenses.forEach(expense => {
      let exist = false;
      let index = -1;
      let type = expense.type;
      if (!type) {
        type = 'Neregistruoti';
      }
      for (let i = 0; i < adjusted.length; i++) {
        if (adjusted[i][0] === type) {
          exist = true;
          index = i;
        }
      }
      if (!exist) {
        adjusted.push([type, expense.value]);
      } else {
        adjusted[index][1] += expense.value;
      }
    });
    return adjusted;
  }

  formatExpenseTypes(expenseTypes) {
    let data = [];
    expenseTypes.forEach((expenseType) => {
      data.push({
        label: expenseType.name,
        value: expenseType._id,
      });
    });
    return data;
  }

  handleChange(value) {
    this.expenseStore.setTypeSelection(value);
  }

  handleCheckAll(value, checked) {
    const { expenseTypes } = this.expenseStore;
    const expenseTypeIds = [];
    expenseTypes.forEach(expense => {
      expenseTypeIds.push(expense._id);
    });
    const nextValue = checked ? expenseTypeIds : [];
    this.expenseStore.setTypeSelection(nextValue);
  }

  filterByType() {
    const { expenseFilter, selectedTypes } = this.expenseStore;
    let filters = {};
    if (expenseFilter) {
      filters = Object.assign(filters, expenseFilter);
    }
    this.picker.close();
    if (selectedTypes.length > 0) {
      const filter = {
        type: {
          $in: selectedTypes,
        },
      };
      filters = Object.assign(filters, filter);
    } else {
      if (Object.keys(filters).indexOf('type') !== -1) {
        delete filters.type;
      }
    }
    this.expenseStore.loadAllExpenses(filters, this.userId);
  }

  filterByDate(date) {
    const { expenseFilter } = this.expenseStore;
    let filters = {};
    if (expenseFilter) {
      filters = Object.assign(filters, expenseFilter);
    }
    if (date.length > 0) {
      date[1].setHours(23, 59, 59);
      const filter = {
        date: {
          $gte: date[0],
          $lte: date[1],
        },
      };
      this.expenseStore.setDateSelection(date);
      filters = Object.assign(filters, filter);
    } else {
      if (Object.keys(filters).indexOf('date') !== -1) {
        delete filters.date;
        this.expenseStore.setDateSelection(undefined);
      }
    }
    this.expenseStore.loadAllExpenses(filters, this.userId);
  }

  resetTypeFilter() {
    this.expenseStore.setTypeSelection();
    this.filterByType();
  }

  getActionButtons() {
    const {
      expenseTypes,
      selectedTypes,
      selectedAllTypes = false,
      selectedDate = null,
    } = this.expenseStore;
    const buttons = [];
    buttons.push(
      <FlexboxGrid.Item key="date-filter">
        <DateRangePicker
          locale={this.state.locale}
          ranges={this.state.ranges}
          placeholder="Filtruoti pagal datą"
          placement="bottomEnd"
          defaultValue={selectedDate}
          onChange={this.filterByDate.bind(this)}
        />
      </FlexboxGrid.Item>
    );
    buttons.push(
      <FlexboxGrid.Item key="type-filter">
        <CheckPicker
          placeholder="Filtruoti pagal tipą"
          ref={ref => {
            this.picker = ref;
          }}
          style={{ width: 170 }}
          placement="bottomEnd"
          value={selectedTypes}
          data={this.formatExpenseTypes(expenseTypes)}
          onChange={this.handleChange}
          onClean={this.resetTypeFilter}
          renderExtraFooter={() => (
            <FlexboxGrid align='middle' justify="space-between">
              <FlexboxGrid.Item>
                <Checkbox
                  inline
                  onChange={this.handleCheckAll}
                  checked={selectedAllTypes}
                >
                  Pažymėti visus
              </Checkbox>
              </FlexboxGrid.Item>
              <FlexboxGrid.Item>
                <Button
                  appearance="primary"
                  style={{ margin: 5 }}
                  size="sm"
                  onClick={this.filterByType.bind(this)}
                >
                  Gerai
                </Button>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          )}
        />
      </FlexboxGrid.Item>
    );
    return buttons;
  }

  render() {
    const { expenses, total } = this.expenseStore;
    return (
      <Content>
        <EditExpenseModal ref={this.editExpenseModal} />
        <FlexboxGrid justify='end' style={{ marginRight: 25, marginTop: 10, marginBottom: 0 }}>
          {this.getActionButtons()}
        </FlexboxGrid>
        <Grid fluid>
          <Row className="show-grid">
            <Col xs={16}>
              <Panel header="Išlaidos" bordered>
                <Table
                  data={this.formatExpenseData(expenses)}
                  height={600}
                  onRowClick={this.handleRowClick.bind(this)}
                >
                  <Column align="center" flexGrow={1} name="asd">
                    <HeaderCell>Data</HeaderCell>
                    <Cell dataKey="date" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Tipas</HeaderCell>
                    <Cell dataKey="type" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Tiekėjas</HeaderCell>
                    <Cell dataKey="provider" />
                  </Column>
                  <Column align="center" flexGrow={1} minWidth={150}>
                    <HeaderCell>Dokumento numeris</HeaderCell>
                    <Cell dataKey="docNumber" />
                  </Column>
                  <Column align="center" fixed="right" flexGrow={1}>
                    <HeaderCell>Suma</HeaderCell>
                    <Cell dataKey="value" />
                  </Column>
                </Table>
                <Col
                  xs={6}
                  xsOffset={18}
                  style={{ background: '#fff', fontSize: '16px' }}
                >
                  <Col xs={7}>Iš viso:</Col>
                  <Col xs={14} align="center">
                    <a href="/company/expenses">{total}</a>
                  </Col>
                </Col>
              </Panel>
            </Col>
            <Col xs={8}>
              <Panel header="Statistiniai Duomenys" bordered>
                <Chart
                  chartType="PieChart"
                  height={'300px'}
                  loader={<Loader speed="fast" content="Kraunama.." />}
                  data={this.formatChartData(expenses)}
                  options={{
                    title: 'Išlaidų grafikas',
                    is3D: true,
                  }}
                />
              </Panel>
            </Col>
          </Row>
        </Grid>
      </Content >
    );
  }
}

Company.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(Company);