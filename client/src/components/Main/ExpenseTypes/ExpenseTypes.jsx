import React, { Component } from 'react';
import {
  Table,
  Content,
  Panel,
  Grid,
  Row,
  Col,
  Checkbox,
  FlexboxGrid,
  Icon,
  IconButton,
} from 'rsuite';

import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import CreateExpenseTypeModal from './CreateExpenseTypeModal';
import EditExpenseTypeModal from './EditExpenseTypeModal';

const { Column, Cell, HeaderCell } = Table;

const StatusCell = ({ rowData, dataKey, ...props }) => (
  <Cell {...props} style={{ padding: 0 }}>
    <div style={{ lineHeight: '46px' }}>
      <Checkbox inline checked={rowData[dataKey]} />
    </div>
  </Cell>
);

class ExpenseTypes extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.formatData = this.formatData.bind(this);
    this.createExpenseTypeModal = React.createRef();
    this.editExpenseTypeModal = React.createRef();
    this.openCreateExpenseTypeModal = this.openCreateExpenseTypeModal.bind(this);
    this.openEditExpenseTypeModal = this.openEditExpenseTypeModal.bind(this);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
  }

  openCreateExpenseTypeModal() {
    this.expenseStore.resetExpenseType();
    this.createExpenseTypeModal.current.open();
  }

  openEditExpenseTypeModal() {
    this.editExpenseTypeModal.current.open();
  }

  componentDidMount() {
    this.expenseStore.loadAllExpenseTypes(this.userId);
  }

  formatDate(date) {
    let dateFormat = '';
    if (date) {
      date = new Date(date);
      dateFormat += date.getFullYear() + '/';
      dateFormat += (date.getMonth() + 1) + '/';
      dateFormat += date.getDate();
    }
    return dateFormat;
  }

  formatData(expenseTypes) {
    let data = [];
    expenseTypes.forEach((expenseType, index) => {
      let date = this.formatDate(expenseType.createdAt);
      data.push({
        id: expenseType._id,
        key: index,
        name: expenseType.name,
        createdAt: date,
        enabled: expenseType.enabled,
      });
    });
    return data;
  }

  handleRowClick(data, event) {
    if (event.target.innerText) {
      this.expenseStore.setExpenseTypeEditData(data);
      this.openEditExpenseTypeModal();
    }
  }

  handleDeleteItem(data) {
    this.expenseStore.deleteExpenseType(data.id, this.userId).then((data) => {
      if (data && !data.errors) {
        this.expenseStore.loadAllExpenseTypes(this.userId);
      } else {
        //TODO: notification
      }
    });
  }

  render() {
    const { expenseStore } = this.props;
    const { expenseTypes } = expenseStore;
    return (
      <Content>
        <CreateExpenseTypeModal ref={this.createExpenseTypeModal} />
        <EditExpenseTypeModal ref={this.editExpenseTypeModal} />
        <Grid fluid>
          <FlexboxGrid
            justify='end'
            style={{
              marginRight: 25,
              marginTop: 10,
              marginBottom: 0,
            }}>
            <FlexboxGrid.Item>
              <IconButton
                style={{ margin: 20 }}
                icon={<Icon icon="plus-square" />}
                placement="left"
                onClick={this.openCreateExpenseTypeModal}
              >
                Registruoti išlaidų tipą
              </IconButton>
            </FlexboxGrid.Item>
          </FlexboxGrid>
          <Row className="show-grid">
            <Col xs={12} xsOffset={6}>
              <Panel header="Išlaidų tipai" bordered>
                <Table
                  data={this.formatData(expenseTypes)}
                  height={500}
                  onRowClick={this.handleRowClick.bind(this)}
                >
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Pavadinimas</HeaderCell>
                    <Cell dataKey="name" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Sukūrimo data</HeaderCell>
                    <Cell dataKey="createdAt" />
                  </Column>
                  <Column align="center" flexGrow={1}>
                    <HeaderCell>Aktyvus</HeaderCell>
                    <StatusCell dataKey="enabled" />
                  </Column>
                  <Column align="center" width={120} fixed="right" verticalAlign="middle">
                    <HeaderCell>Veiksmai</HeaderCell>
                    <Cell className="link-group" style={{ padding: 0 }}>
                      {rowData => {
                        return (
                          <IconButton
                            icon={<Icon icon="trash2" style={{ color: '#f44336' }} />}
                            circle size="sm"
                            name="delete"
                            onClick={this.handleDeleteItem.bind(this, rowData)}
                          />
                        );
                      }}
                    </Cell>
                  </Column>
                </Table>
              </Panel>
            </Col>
          </Row>
        </Grid>
      </Content >
    );
  }
}

ExpenseTypes.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(ExpenseTypes);