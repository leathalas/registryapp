import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { compose } from 'recompose';
import PropTypes from 'prop-types';

import {
  Form,
  FormGroup,
  ControlLabel,
  FormControl,
} from 'rsuite';

class ExpenseTypesCreateForm extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.form = this.props.form;
    this.model = this.props.model;
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setExpenseTypeCheckboxValue = this.setExpenseTypeCheckboxValue.bind(this);
  }

  handleInputChange(value) {
    this.expenseStore.setExpenseTypeFormData(value);
  }

  setExpenseTypeCheckboxValue(field) {
    this.expenseStore.setExpenseTypeCheckboxValue(field);
  }

  render() {
    const { expenseType } = this.expenseStore;
    return (
      <Form
        fluid
        onSubmit={this.handleSubmit}
        ref={this.form}
        model={this.model}
        formValue={expenseType}
      >
        <FormGroup>
          <ControlLabel>Išlaidų tipas</ControlLabel>
          <FormControl onChange={this.handleInputChange.bind(this)} name="name" />
        </FormGroup>
      </Form>
    );
  }
}

ExpenseTypesCreateForm.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(ExpenseTypesCreateForm);