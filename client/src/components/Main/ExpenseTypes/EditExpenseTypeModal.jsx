import React, { Component } from 'react';
import { compose } from 'recompose';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';

import Modal from '../../Partials/Modal';
import ExpenseTypesEditFrom from './ExpenseTypesEditForm';
import ReactNotification from 'react-notifications-component';
import { store } from 'react-notifications-component';
import { Schema } from 'rsuite';

const { StringType } = Schema.Types;

class EditExpenseTypeModal extends Component {
  constructor(props) {
    super(props);
    this.expenseStore = this.props.expenseStore;
    this.modal = React.createRef();
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.onSave = this.onSave.bind(this);
    this.userId = window.sessionStorage.getItem('userId');
    this.model = Schema.Model({
      name: StringType()
        .isRequired('Prašom įvesti tipą'),
    });
    this.form = React.createRef()
  }

  open() {
    this.modal.current.open();
  }

  close() {
    this.modal.current.close();
  }

  onSave() {
    if (!this.form.current.check()) {
      return;
    }
    this.expenseStore.updateExpenseTypeData(this.userId).then(data => {
      if (data && !data.errors) {
        this.expenseStore.loadAllExpenseTypes(this.userId);
        this.expenseStore.resetExpense();
        this.close();
      } else {
        store.addNotification({
          title: "Klaida!",
          message: data.errors,
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 2000,
            onScreen: true,
          },
        });
      }
    });
  }

  render() {
    return (
      <div>
        <ReactNotification />
        <Modal
          title="Išlaidų tipo redagavimas"
          ref={this.modal}
          onSubmit={this.onSave}
          submitName="Išsaugoti"
          closeName='Uždaryti'
        >
          <ExpenseTypesEditFrom model={this.model} form={this.form} />
        </Modal>
      </div>
    );
  }
}

EditExpenseTypeModal.propTypes = {
  expenseStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('expenseStore'), observer);

export default enhance(EditExpenseTypeModal);
