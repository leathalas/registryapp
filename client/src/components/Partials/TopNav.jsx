import React, { Component } from 'react';
import { Navbar, Icon, Nav, Header, Dropdown } from 'rsuite';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { compose } from 'recompose';
import { NavLink } from 'react-router-dom';

class TopNav extends Component {
  constructor(props) {
    super(props);
    this.getActionButtons = this.getActionButtons.bind(this);
    this.getMainButtons = this.getMainButtons.bind(this);
  }

  handleSignOut() {
    const { sessionStorage, location } = window;
    sessionStorage.setItem('token', '');
    location.href = '/';
  }

  getActionButtons() {
    const { sessionStorage } = window;
    const token = sessionStorage.getItem('token');
    if (!token) {
      return (
        <Nav.Item
          icon={<Icon icon="sign-in" />}
          componentClass={NavLink}
          to="/sign-in"
        >
          Prisijungti
        </Nav.Item>
      );
    } else {
      return (
        <Dropdown icon={<Icon icon="cog" />} title="Nustatymai">
          <Dropdown.Item
            icon={<Icon icon="sign-out" />}
            onClick={this.handleSignOut}
            componentClass={NavLink}
            to="/"
          >
            Atsijungti
          </Dropdown.Item>
          <Dropdown.Item
            icon={<Icon icon="file" />}
            componentClass={NavLink}
            to="/company/expense/types"
          >
            Išlaidų tipai
          </Dropdown.Item>
        </Dropdown>
      );
    }
  }

  getMainButtons() {
    const { sessionStorage } = window;
    const token = sessionStorage.getItem('token');
    if (token) {
      return (
        <Nav.Item componentClass={NavLink} to="/company/expenses">
          Išlaidos
        </Nav.Item>
      );
    }
  }

  render() {
    return (
      <Header>
        <Navbar className="nav-dark">
          <Navbar.Body>
            <Nav>
              <Nav.Item componentClass={NavLink} to="/" icon={<Icon icon="home" />}>
                Pagrindinis
              </Nav.Item>
              {this.getMainButtons()}
            </Nav>
            <Nav pullRight>
              {this.getActionButtons()}
            </Nav>
          </Navbar.Body>
        </Navbar>
      </Header>
    );
  }
}

TopNav.propTypes = {
  history: PropTypes.object,
};

const enhance = compose(observer);

export default enhance(TopNav);