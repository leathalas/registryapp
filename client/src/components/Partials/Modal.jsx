import React from 'react';
import { Modal as RsuiteModal, Button } from 'rsuite';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    this.close = this.close.bind(this);
    this.open = this.open.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  close() {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
    this.setState({ show: false });
  }

  open() {
    const { onOpen } = this.props;
    if (onOpen) {
      onOpen();
    }
    this.setState({ show: true });
  }

  onSubmit() {
    const { onSubmit } = this.props;
    if (onSubmit) {
      return onSubmit();
    }
    this.close();
  }

  render() {
    const { show } = this.state;
    const {
      children,
      title = '',
      submitName = 'Išsaugoti',
      closeName = 'Uždaryti',
    } = this.props;
    return (
      <RsuiteModal show={show} onHide={this.close}>
        <RsuiteModal.Header>
          <RsuiteModal.Title>{title}</RsuiteModal.Title>
        </RsuiteModal.Header>
        <RsuiteModal.Body>
          {children}
        </RsuiteModal.Body>
        <RsuiteModal.Footer>
          <Button onClick={this.onSubmit.bind(this)} appearance="primary">
            {submitName}
          </Button>
          <Button onClick={this.close} appearance="default">
            {closeName}
          </Button>
        </RsuiteModal.Footer>
      </RsuiteModal>
    );
  }
}

export default Modal;