import React, { Component } from 'react';
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import jwt_decode from 'jwt-decode';
import {
  Form,
  Button,
  ButtonToolbar,
  FormGroup,
  ControlLabel,
  FormControl,
  Content,
  FlexboxGrid,
  Panel,
  Schema,
  Loader,
} from 'rsuite';

import ReactNotification from 'react-notifications-component';
import { store } from 'react-notifications-component';

const { StringType } = Schema.Types;

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.form = React.createRef();
    this.model = Schema.Model({
      email: StringType()
        .isEmail('Prašom patikslinti savo el. paštą.')
        .isRequired('Užpildykite laukelį.'),
      password: StringType().isRequired('Užpildykite laukelį.'),
      company: StringType().isRequired('Užpildykite laukelį.'),
    });
    this.handleRegister = this.handleRegister.bind(this);
    this.state = {
      loading: false,
    };
  }

  handleRegister() {
    const { usersStore, history } = this.props;
    const { sessionStorage } = window;
    if (!this.form.current.check()) {
      return;
    }
    this.setState({ loading: true });
    usersStore.userRegister().then(data => {
      this.setState({ loading: false });
      if (data && data.token) {
        sessionStorage.setItem('token', data.token);
        sessionStorage.setItem('userId', jwt_decode(data.token).user._id);
        history.push('/');
      } else {
        store.addNotification({
          title: "Klaida!",
          message: data.errors,
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 2000,
            onScreen: true,
          },
        });
      }
    });
  }

  render() {
    const { usersStore } = this.props;
    return (
      <Content>
        <ReactNotification />
        <FlexboxGrid justify="center">
          <FlexboxGrid.Item colspan={12} className="auth-wrapper">
            <Panel header={<h3>Registracija</h3>} bordered className='auth-form'>
              <Form
                fluid
                model={this.model}
                ref={this.form}
                onSubmit={this.handleRegister}
                onChange={usersStore.setRegisterData}
              >
                <FormGroup>
                  <ControlLabel>El. pašto adresas</ControlLabel>
                  <FormControl name="email" />
                </FormGroup>
                <FormGroup>
                  <ControlLabel>Įmonės pavadinimas</ControlLabel>
                  <FormControl name="company" />
                </FormGroup>
                <FormGroup>
                  <ControlLabel>Slaptažodis</ControlLabel>
                  <FormControl name="password" type="password" />
                </FormGroup>
                <FormGroup>
                  {this.state.loading && <div><Loader content="Krauname..." /><hr /></div>}
                  <ButtonToolbar>
                    <Button appearance="primary" type="submit">Sukūrti</Button>
                    <Button appearance="link" href='/sign-in'>Jau turite paskyrą?</Button>
                  </ButtonToolbar>
                </FormGroup>
              </Form>
            </Panel>
          </FlexboxGrid.Item>
        </FlexboxGrid>
      </Content>
    );
  }
}

SignUp.propTypes = {
  usersStore: PropTypes.object.isRequired,
  history: PropTypes.object,
};

const enhance = compose(inject('usersStore'), observer);

export default enhance(SignUp);
