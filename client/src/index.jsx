import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { configure } from 'mobx';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'mobx-react-router';
import ConfigHelper from './helpers/ConfigHelper';
import App from './App';

configure({ enforceActions: 'always' });

const stores = ConfigHelper.getStores();
const routes = ConfigHelper.getRoutes();

const browserHistory = createBrowserHistory();
const appHistory = syncHistoryWithStore(browserHistory, stores.routerStore);

ReactDOM.render(
  <Provider {...stores}>
    <Router history={appHistory}>
      <App routes={routes} />
    </Router>
  </Provider >,
  document.getElementById('root')
);
