import RouterStore from './stores/RouterStore';
import UsersStore from './stores/UsersStore';
import ExpenseStore from './stores/ExpenseStore';
import SignIn from './components/Users/SignIn';
import SignUp from './components/Users/SignUp';
import Expenses from './components/Main/Expenses/Expenses';
import ExpenseTypes from './components/Main/ExpenseTypes/ExpenseTypes';
import Company from './components/Main/Company/Company';

const enabled = true;

const stores = {
  routerStore: RouterStore,
  usersStore: UsersStore,
  expenseStore: ExpenseStore,
};

const routes = [
  {
    path: '/sign-in',
    component: SignIn,
    authenticated: false,
  },
  {
    path: '/sign-up',
    component: SignUp,
    authenticated: false,
  },
  {
    path: '/company/expenses',
    component: Expenses,
    authenticated: true,
  },
  {
    path: '/',
    component: SignIn,
    authenticated: false,
    exact: true,
  },
  {
    path: '/',
    component: Company,
    authenticated: true,
    exact: true,
  },
  {
    path: '/company/expense/types',
    component: ExpenseTypes,
    authenticated: true,
  },
];

export { routes, stores, enabled };
