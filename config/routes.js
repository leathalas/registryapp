const express = require('express');
const router = express.Router();
const UserController = require('../app/controllers/UsersController');
const ExpenseTypesController = require('../app/controllers/ExpenseTypesController');

const UserHelper = require('../app/helpers/UserHelper');
const ExpenseController = require('../app/controllers/ExpenseController');

router.post('/api/user/login/', UserController.login);
router.post('/api/user/register/', UserHelper.validate('UserRegister'), UserController.register);
router.post('/api/expense/types/create', ExpenseTypesController.createType);
router.put('/api/expense/types/update', ExpenseTypesController.updateType);
router.get('/api/expense/types/findAll', ExpenseTypesController.getAll);
router.post('/api/expense/types/delete', ExpenseTypesController.deleteType);
router.post('/api/expense/create', ExpenseController.createExpense);
router.get('/api/expense/findAll', ExpenseController.getAll);
router.post('/api/expense/delete', ExpenseController.deleteExpense);
router.put('/api/expense/update', ExpenseController.updateExpense);

module.exports = router;